import React from "react";
import "./App.css";
import EmployeePage from "./pages/empPage";

function App() {
  return (
    <div className="container">
      <div className="row">
        <div className="col">
          <EmployeePage />
        </div>
      </div>
    </div>
  );
}

export default App;
