
create table emptb (
  empid integer primary key auto_increment,
  emp_name VARCHAR(200),
  salary FLOAT,
  age integer
);

insert into emptb (emp_name, salary, age) values ('prafulla', 20000, 24);
insert into emptb (emp_name, salary, age) values ('tejas', 30000, 25);
insert into emptb (emp_name, salary, age) values ('shivam', 40000, 23);
insert into emptb (emp_name, salary, age) values ('vishal', 50000, 22);